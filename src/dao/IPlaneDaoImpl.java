package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Planes;

public class IPlaneDaoImpl implements IPlaneDao {

	@Override
	public void insertPlanes(Planes planes) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.save(planes);
		tx.commit();

	}

	@Override
	public List<Planes> view() {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		List<Planes> list = new ArrayList<Planes>();
		Query q = session.createQuery("from Planes");
		List<Planes> l = q.list();
		for (Planes planes : l) {
			list.add(planes);
		}

		return list;
	}

	@Override
	public void updatePlanes(Planes planes) {
		Session session = new Configuration().configure("cfg.xml").buildSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query q = session.createQuery("update Planes set source=:source,destination=:destination where pid=:pid");
		q.setParameter("source", planes.getSource());
		q.setParameter("destination", planes.getDestination());
		q.setParameter("pid", planes.getPid());
		q.executeUpdate();
		session.save(planes);

	}
}
