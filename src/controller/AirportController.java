package controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.IAdminDao;
import dao.IAdminDaoImpl;
import dao.IHangarDao;
import dao.IHangarDaoImpl;
import dao.IManagerDao;
import dao.IManagerDaoImpl;
import dao.IPilotDao;
import dao.IPilotDaoImpl;
import dao.IPlaneDao;
import dao.IPlaneDaoImpl;
import model.AirportAdmin;
import model.AirportManager;
import model.Hangars;
import model.Pilots;
import model.Planes;

@Controller

public class AirportController {

	IAdminDao dao = new IAdminDaoImpl();
	IHangarDao hdao = new IHangarDaoImpl();
	IManagerDao mdao = new IManagerDaoImpl();
	IPilotDao pidao = new IPilotDaoImpl();
	IPlaneDao pdao = new IPlaneDaoImpl();

	@RequestMapping("adminRegister")
	public ModelAndView adminRegister() {
		return new ModelAndView("adminRegister", "a", new AirportAdmin());
	}

	@RequestMapping("adminAdd")
	public ModelAndView add(@ModelAttribute("a") AirportAdmin a) {
		dao.insertAdmin(a);
		return new ModelAndView("message");
	}

	@RequestMapping("adminLogin")
	public ModelAndView adminLogin() {
		return new ModelAndView("adminLogin", "a", new AirportAdmin());
	}

	@RequestMapping("adminValidate")
	public ModelAndView adminValidate(@ModelAttribute("a") AirportAdmin admin) {
		boolean isValid = dao.adminAuthentication(admin);
		if (isValid) {
			return new ModelAndView("display", "v", "Login successful!!!");
		} else {
			return new ModelAndView("adminLogin", "v", "Please check your Credentials");
		}
	}

	@RequestMapping("addHangars")
	public ModelAndView addHangars() {
		return new ModelAndView("addHangars", "h", new Hangars());
	}

	@RequestMapping("insertHangars")
	public ModelAndView add(@ModelAttribute("h") Hangars hangars) {
		hdao.insertHangars(hangars);
		return new ModelAndView("message");
	}

	@RequestMapping("viewHangars")
	public ModelAndView view() {
		List<Hangars> list = hdao.view();
		return new ModelAndView("viewHangars", "h", list);
	}

	@RequestMapping("managerRegister")
	public ModelAndView managerRegister() {
		return new ModelAndView("managerRegister", "m", new AirportManager());
	}

	@RequestMapping("managerAdd")
	public ModelAndView add(@ModelAttribute("m") AirportManager m) {
		mdao.insertManager(m);
		return new ModelAndView("message");
	}

	@RequestMapping("managerLogin")
	public ModelAndView managerLogin() {
		return new ModelAndView("managerLogin", "m", new AirportManager());
	}

	@RequestMapping("managerValidate")
	public ModelAndView managerValidate(@ModelAttribute("m") AirportManager manager) {
		boolean isValid = mdao.managerAuthentication(manager);
		if (isValid) {
			return new ModelAndView("managerLogin", "v", "Login Successfull!!!");
		} else {
			return new ModelAndView("managerLogin", "v", "Please check your Credentials");
		}
	}

	@RequestMapping("addPilot")
	public ModelAndView addPilots() {
		return new ModelAndView("addPilot", "pi", new Pilots());
	}

	@RequestMapping("insert")
	public ModelAndView add(@ModelAttribute("pi") Pilots pilots) {
		pidao.insertPilots(pilots);
		return new ModelAndView("message");
	}

	@RequestMapping("viewPilot")
	public ModelAndView view1() {
		List<Pilots> list = pidao.view();
		return new ModelAndView("viewPilot", "pi", list);
	}

	@RequestMapping("addPlanes")
	public ModelAndView addPlanes() {
		return new ModelAndView("addPlanes", "p", new Planes());
	}

	@RequestMapping("add")
	public ModelAndView add(@ModelAttribute("p") Planes planes) {
		pdao.insertPlanes(planes);
		return new ModelAndView("message");
	}

	@RequestMapping("viewPlanes")
	public ModelAndView view2() {
		List<Planes> list = pdao.view();
		return new ModelAndView("viewPlanes", "p", list);
	}

}
