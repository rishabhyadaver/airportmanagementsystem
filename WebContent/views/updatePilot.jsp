<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Pilots</title>
</head>
<body>
<h1>Update Pilots</h1>
<a href="display">Back</a>
<s:form action="update" method="post" modelAttribute="u">
Age<s:input path="age" autocomplete="off"></s:input><br/>
Mobile<s:input path="mobile" autocomplete="off"></s:input><br/>
Pilot Id<s:input path="id" autocomplete="off"></s:input><br/>
<s:button>Update</s:button>
</s:form>

</body>
</html>
