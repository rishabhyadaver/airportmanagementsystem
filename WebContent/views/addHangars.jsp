<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Hangars</title>
</head>
<body>
<h1>Add Hangars</h1>
<a href="display">Back</a><br/><br/>
<s:form action="insertHangars" method="post" modelAttribute="h">
Hangar Id<s:input path="hid" autocomplete="off"></s:input><br/>
Hangar Name<s:input path="hname" autocomplete="off"></s:input><br/>
Size<s:input path="size" autocomplete="off"></s:input><br/>
Location<s:input path="location" autocomplete="off"></s:input><br/>
<s:button>Add</s:button>
</s:form>

</body>
</html>
