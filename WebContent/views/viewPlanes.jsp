<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Plane Details</title>
</head>
<body>
<a href="index.jsp">Home</a>
<br/><br/>
<table border="5">
<tr>
<th colspan="5">Plane Details<br/></th></tr>
<tr>
<th>Plane Id</th>
<th>Plane Name</th>
<th>Source</th>
<th>Destination</th>
<th>Capacity</th>
</tr>
<c:forEach items="${p}" var="pl">
<tr style="text-align:center;">
<td>${pl.getPid()}</td>
<td>${pl.getPname()}</td>
<td>${pl.getSource()}</td>
<td>${pl.getDestination()}</td>
<td>${pl.getCapacity()}</td>
<td><a style="text-decoration:none" href="updatePlanes">Update</a></td>
</tr>


</c:forEach>
</table>

</body>
</html>
