<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Planes</title>
</head>
<body>
<h1>Add Planes</h1>
<a href="index.jsp">Home</a><br/><br/>
<s:form action="add" method="post" modelAttribute="p">
Plane Id<s:input path="pid" autocomplete="off"></s:input><br/>
Plane Name<s:input path="pname" autocomplete="off"></s:input><br/>
Source<s:input path="source" autocomplete="off"></s:input><br/>
Destination<s:input path="destination" autocomplete="off"></s:input><br/>
Capacity<s:input path="capacity" autocomplete="off"></s:input><br/>
<s:button>Add</s:button>
</s:form>

</body>
</html>
